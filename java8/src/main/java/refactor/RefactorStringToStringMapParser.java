package refactor;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


class RefactorStringToStringMapParser
    implements RefactorMapParser< String, String > {
    
    static Map< String, String > parseString( String mapString ) {
        return new RefactorStringToStringMapParser().parse( mapString );
    }
    
    
    @Override
    public String parseValue( Entry entry ) {
        return entry.getValue();
    }
    
    
    @Override
    public String parseKey( Entry entry ) {
        return entry.getKey();
    }
    
}
