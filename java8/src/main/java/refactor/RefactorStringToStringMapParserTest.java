package refactor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


class RefactorStringToStringMapParserTest {
    
    @Test
    void parseNull_returnsEmptyMap() {
        Assertions.assertTrue( RefactorStringToStringMapParser.parseString( null ).isEmpty() );
    }
    
    @Test
    void parseEmptyString_returnsEmptyMap() {
        Assertions.assertTrue( RefactorStringToStringMapParser.parseString( "" ).isEmpty() );
    }
    
    @Test
    void parseStringWithSingleMappping_returnsValidMap() {
        var map = RefactorStringToStringMapParser.parseString( "key -> value" );
        Assertions.assertEquals( 1, map.size() );
        Assertions.assertEquals( "value", map.get( "key" ) );
    }
    
    @Test
    void parseStringWitTwoMapppings_returnsValidMap() {
        var map = RefactorStringToStringMapParser.parseString( "key1 -> value1, key2 -> value2" );
        Assertions.assertEquals( 2, map.size() );
        Assertions.assertEquals( "value1", map.get( "key1" ) );
        Assertions.assertEquals( "value2", map.get( "key2" ) );
    }
    
    @Test
    void parseStringWithEmptyMappings_returnsValidMap() {
        var map = RefactorStringToStringMapParser.parseString( "key1 -> , key2 -> , ->" );
        Assertions.assertEquals( 3, map.size() );
        Assertions.assertEquals( "", map.get( "key1" ) );
        Assertions.assertEquals( "", map.get( "key2" ) );
        Assertions.assertEquals( "", map.get( "" ) );
    }
    
    @Test
    void parseStringWitBajillionMapppings_returnsValidMap() {
        var map = createSimpleMap();
        var generatedMap = RefactorStringToStringMapParser.parseString( mapToString( map ) );
        assertMaps( map, generatedMap );
    }
    
    
    private void assertMaps( Map< String, String > map, Map< String, String > generatedMap ) {
        Assertions.assertAll( map.entrySet().stream()
            .map( entry -> (Executable) () -> Assertions.assertEquals( entry.getValue(), generatedMap.get( entry.getKey() ) ) )
            .collect( Collectors.toList() ) );
    }
    
    
    private String mapToString( Map< String, String > map ) {
        return map.entrySet().stream()
            .map( entry -> entry.getKey()+" -> " + entry.getValue() )
            .collect( StringBuilder::new , this::append, this::append )
            .toString();
    }
    
    
    private Map< String, String > createSimpleMap() {
        return IntStream.range( 0, 10_000 ).boxed()
            .collect( Collectors.toMap( i -> "key" + i, i -> "value" + i ) );
    }
    
    
    private void append( StringBuilder sb1, StringBuilder sb2 ) {
        if( sb1.length() != 0 && sb2.length() != 0 )
            sb1.append( ", " );
        sb1.append( sb2 );
    }
    
    
    private void append( StringBuilder sb, String s ) {
        if( sb.length() != 0 )
            sb.append( ", " );
        sb.append( s );
    }
}
