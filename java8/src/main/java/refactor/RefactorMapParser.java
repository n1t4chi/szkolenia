package refactor;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public interface RefactorMapParser< K, V > {
    Pattern PATTERN = Pattern.compile( "(.*?)->(.*)" );
    
    default Map< K, V > parse( String mapString ) {
        return mapString == null
            ? Collections.emptyMap()
            : parseStringToMap( mapString );
    }
    
    V parseValue( Entry entry );
    
    K parseKey( Entry entry );
    
    private Map< K, V > parseStringToMap( String mapString ) {
        return Arrays
            .stream( mapString.split( "," ) )
            .map( String::strip )
            .map( PATTERN::matcher )
            .filter( Matcher::find )
            .map( this::matcherToEntry )
            .collect( Collectors.toMap( this::parseKey, this::parseValue ) )
        ;
    }
    
    private Entry matcherToEntry( Matcher matcher ) {
        return new Entry( matcher.group( 1 ).strip(), matcher.group( 2 ) .strip());
    }
    
    class Entry {
        private final String key;
        private final String value;
        
        
        Entry( String key, String value ) {
            this.key = key;
            this.value = value;
        }
        
        
        String getKey() {
            return key;
        }
        
        
        String getValue() {
            return value;
        }
    }
}
