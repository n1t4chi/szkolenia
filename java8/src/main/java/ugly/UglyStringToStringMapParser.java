package ugly;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class UglyStringToStringMapParser {
    
    static Map< String, String > parseString( String mapString ) {
        Map< String, String > map = new HashMap<>();
        Pattern pattern = Pattern.compile( "(.*?)->(.*)" );
        if( mapString != null ) {
            String[] split = mapString.split( "," );
            for( int i = 0; i < split.length; i++ ) {
                String s = split[i];
                String strip = s.strip();
                Matcher matcher = pattern.matcher( strip );
                if( matcher.find() ) {
                    String key = matcher.group( 1 ).strip();
                    String value = matcher.group( 2 ).strip();
                    map.put( key, value );
                }
            }
        }
        return map;
    }
    
}
