/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package Streams;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


class Primes {
    
    
    private List< Integer > primesFor( int n ) {
        if( n < 2 ) {
            return Collections.emptyList();
        }
        
        List< Integer > primes = new ArrayList<>();
        primes.add( 2 );
        
        for( int number = 3; number <= n; number += 2 ) {
            boolean isPrime = true;
            int squareRoot = (int) Math.ceil( Math.sqrt( number ) );
            for( Integer prime : primes ) {
                if( squareRoot < prime ) {
                    break;
                }
                if( number % prime == 0 ) {
                    isPrime = false;
                    break;
                }
            }
            if( isPrime ) {
                primes.add( number );
            }
        }
        return primes;
    }
    
    
    private List< Integer > primesStream( int n ) {
        if( n < 2 ) {
            return Collections.emptyList();
        }
        
        List< Integer > primes = new ArrayList<>();
        primes.add( 2 );
        IntStream.iterate( 3, i -> i <= n , i -> i+2 )
            .filter( number -> {
                int squareRoot = (int) Math.ceil( Math.sqrt( number ) );
                return primes.stream()
                    .takeWhile( prime -> prime <= squareRoot )
                    .noneMatch( prime -> number % prime == 0 );
            } )
            .forEach( primes::add );
        return primes;
    }
    
    private static AtomicInteger start = new AtomicInteger( 3 );
    private static List< Integer > primes = new ArrayList<>();
    
    static {
        primes.add( 2 );
        primes.add( 3 );
    }
    
    private List< Integer > primesStream2( int n ) {
        if( start.get() >= n ) {
            return primes.stream()
                .takeWhile( number -> number <= n )
                .collect( Collectors.toList());
        }
        int limit = n - 1 + n%2;
        IntStream.generate( () -> start.addAndGet( 2 ) )
            .filter( number -> {
                int squareRoot = (int) Math.ceil( Math.sqrt( number ) );
                return primes.stream()
                    .takeWhile( prime -> prime <= squareRoot )
                    .noneMatch( prime -> number % prime == 0 );
            } )
            .takeWhile( number -> number <= limit )
            .forEach( primes::add );
        start.set( limit );
        
        return new ArrayList<>( primes );
    }
    
    
    @Test
    void testPrimes() {
        for( int i = 0; i < 25; i++ ) {
            System.err.println( i + " -> " + primesFor( i ) + " / " + primesStream2( i ) );
        }
    }
    
    @Test
    void testPrimes1() {
        for( int i = 0; i < 5525; i++ ) {
            primesFor( i );
        }
    }
    
    @Test
    void testPrimes2() {
        for( int i = 0; i < 5525; i++ ) {
            primesStream( i );
        }
    }
    
    @Test
    void testPrimes3() {
        for( int i = 0; i < 5525; i++ ) {
            primesStream2( i );
        }
    }
    
    @Test
    void testStream() {
        System.out.println(
            Arrays.toString(
                IntStream.iterate( 0, i -> i < 10000, i -> i + 1 )
                    .boxed()
                    .parallel()
                    .filter( this::checkFilter )
                    .collect( Collectors.toList() )
                    .stream()
                    .sequential()
                    .takeWhile( this::checkWhile )
                    .toArray()
            )
        );
    }
    
    
    private boolean checkWhile( int i ) {
        if( i>= 30) {
            System.err.println( "while "+i );
        }
        return i < 30;
    }
    
    
    private boolean checkFilter( int i ) {
        if( i >= 10) {
            System.err.println( "filter "+i );
        }
        
        return i < 10;
    }
}
