/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package Streams;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.stream.IntStream;


class Factorial {
    
    private BigInteger factorialFor( int n ) {
        BigInteger factorial = BigInteger.ONE;
        for( int i = 2; i <= n; i++ ) {
            factorial = factorial.multiply( BigInteger.valueOf( i ) );
        }
        return factorial;
    }
    
    
    private BigInteger factorialStream( int n ) {
        return IntStream.rangeClosed( 2, n )
            .parallel()
            .mapToObj( BigInteger::valueOf )
            .reduce( BigInteger.ONE, BigInteger::multiply );
    }
    
    
    @Test
    void testFactorials() {
        for( int i = 0; i < 25; i++ ) {
            System.err.println( i + " -> " + factorialFor( i ) + " / " + factorialStream( i ) );
        }
    }
    
}
