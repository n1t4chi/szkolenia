package bautiful;

import java.util.*;


class StringToStringMapParser implements StringToMapParser< String, String > {
    
    static Map< String, String > parseString( String mapString ) {
        return new StringToStringMapParser().parse( mapString );
    }
    
    @Override
    public String processValue( String key, String value ) {
        return value;
    }
    
    @Override
    public String processKey( String key, String value ) {
        return key;
    }
}
