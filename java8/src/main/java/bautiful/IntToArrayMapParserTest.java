package bautiful;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IntToArrayMapParserTest {
    
    @Test
    void parseStringWithSingleMappping_returnsValidMap() {
        var map = IntToArrayMapParser.parseString( "1 -> [1 2 3 4], 2->[5 6 7 8],3->[1 3 5 7]" );
        Assertions.assertEquals( 3, map.size() );
        Assertions.assertArrayEquals( new Integer[] { 1, 2, 3, 4 }, map.get( 1 ) );
        Assertions.assertArrayEquals( new Integer[] { 5, 6, 7, 8 }, map.get( 2 ) );
        Assertions.assertArrayEquals( new Integer[] { 1, 3, 5, 7 }, map.get( 3 ) );
    }
}
