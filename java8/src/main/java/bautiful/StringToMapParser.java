package bautiful;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public interface StringToMapParser< K, V > {
    Pattern ENTRY_PATTERN = Pattern.compile( "(.*?)->(.*)" );
    
    default Map< K, V > parse( String mapString ) {
        return mapString == null
            ? Collections.emptyMap()
            : splitStringIntoMap( mapString );
    }
    
    V processValue( String key, String value );
    
    K processKey( String key, String value );
    
    private Map< K, V > splitStringIntoMap( String mapString ) {
        return Arrays
            .stream( mapString.split( "," ) )
            .map( String::strip )
            .map( ENTRY_PATTERN::matcher )
            .filter( Matcher::find )
            .map( this::matchertoEntry )
            .collect( Collectors.toMap(
                entry -> processKey( entry.getKey(), entry.getValue() ),
                entry -> processValue( entry.getKey(), entry.getValue() )
            ) )
            ;
    }
    
    private Entry matchertoEntry( Matcher matcher ) {
        return new Entry( matcher.group( 1 ), matcher.group( 2 ) );
    }
    
    class Entry {
        private final String key;
        private final String value;
        
        
        private Entry( String key, String value ) {
            this.key = key.strip();
            this.value = value.strip();
        }
        
        
        private String getKey() {
            return key;
        }
        
        
        private String getValue() {
            return value;
        }
    }
}
