package bautiful;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Stream;


public class IntToArrayMapParser implements StringToMapParser<Integer, Integer[]> {
    
    static Map< Integer, Integer[] > parseString( String s ) {
        return new IntToArrayMapParser().parse( s );
    }
    
    
    @Override
    public Integer[] processValue( String key, String value ) {
        if( !value.matches( "\\[(\\d*\\s*)*]" ) ) {
            throw new IllegalArgumentException( "Cannot parse value: " + value );
        }
        return splitValueToStream( value )
            .map( Integer::parseInt )
            .toArray( Integer[]::new );
    }
    
    
    private Stream< String > splitValueToStream( String value ) {
        return Arrays.stream( splitValueString( value ) );
    }
    
    
    private String[] splitValueString( String value ) {
        return value.substring( 1, value.length()-1 ).split( "\\s+" );
    }
    
    
    @Override
    public Integer processKey( String key, String value ) {
        return Integer.parseInt( key );
    }
}
