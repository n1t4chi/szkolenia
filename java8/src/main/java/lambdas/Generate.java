/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas;

import org.junit.jupiter.api.Test;

import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;


class Generate {
    
    IntUnaryOperator INCREMENT = j -> j + 1;
    
    @Test
    void generateFunctions() {
        IntStream.iterate( 4, lesserOrEqualThan( 10 ), INCREMENT )
            .forEach( i -> {
                StringBuilder genericTypes = new StringBuilder(  );
                IntStream.iterate( 1, lesserOrEqualThan( i ), INCREMENT )
                    .forEach( j -> genericTypes.append( ", P" ).append( j ) );
                
                StringBuilder reductions = new StringBuilder(  );
                IntStream.iterate( 1, lesserOrEqualThan( i ), INCREMENT ).forEach( j -> {
                    StringBuilder genericFunctionTypes = new StringBuilder(  );
                    IntStream.iterate( 1, lesserOrEqualThan( i ), INCREMENT )
                        .filter( k -> k != j )
                        .forEach( k -> genericFunctionTypes.append( ", P" ).append( k ) );
                    
                    StringBuilder functionParams = new StringBuilder(  );
                    functionParams.append( "P" ).append( j ).append( " param" ).append( j );
                    
                    StringBuilder lambdaParams = new StringBuilder(  );
                    IntStream.iterate( 1, lesserOrEqualThan( i ), INCREMENT )
                        .filter( k -> k != j )
                        .forEach( k -> lambdaParams.append( ", param" ).append( k ) );
                    if( lambdaParams.length() >= 2 ) {
                        lambdaParams.replace( 0, 2, "" );
                    }
                    
                    StringBuilder lambdaCallParams = new StringBuilder(  );
                    IntStream.iterate( 1, lesserOrEqualThan( i ), INCREMENT )
                        .forEach( k -> lambdaCallParams.append( ", param" ).append( k ) );
                    if( lambdaCallParams.length() >= 2 ) {
                        lambdaCallParams.replace( 0, 2, "" );
                    }
                    
                    StringBuilder function = new StringBuilder(  );
                    function
                        .append( "        default Function" ).append( i - 1 ).append( "< R" ).append( genericFunctionTypes ).append( " > reduce" ).append( j ).append( "( " ).append( functionParams ).append( " ) {\n" )
                        .append( "             return ( " ).append( lambdaParams ).append( " ) -> apply( " ).append( lambdaCallParams ).append( " );\n" )
                        .append( "        }\n" );
                    
                    reductions.append( function );
                } );
                
                StringBuilder applyParams = new StringBuilder(  );
                IntStream.iterate( 1, lesserOrEqualThan( i ), INCREMENT )
                    .forEach( k -> applyParams.append( ", P" ).append( k ).append( " param" ).append( k ) );
                if( applyParams.length() >= 2 ) {
                    applyParams.replace( 0, 2, "" );
                }
                
                System.out.println(
                    "    \n" +
                        "    \n" +
                        "     interface Function" + i + "< R"+ genericTypes +" > {\n" +
                        "        R apply( " + applyParams + " );\n" +
                        "        \n" +
                        reductions
                        +"    }"
                );
            } );
    }
    
    
    private IntPredicate lesserOrEqualThan( int i ) {
        return j -> j <= i;
    }
}
