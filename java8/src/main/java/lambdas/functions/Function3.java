/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function3< R, P1, P2, P3 > {
    R apply( P1 param1, P2 param2, P3 param3 );
    
    default Function2< R, P2, P3 > reduce1( P1 param1 ) {
        return ( param2, param3 ) -> apply( param1, param2, param3 );
    }
    
    default Function2< R, P1, P3 > reduce2( P2 param2 ) {
        return ( param1, param3 ) -> apply( param1, param2, param3 );
    }
    
    default Function2< R, P1, P2 > reduce3( P3 param3 ) {
        return ( param1, param2 ) -> apply( param1, param2, param3 );
    }
}
