/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function4< R, P1, P2, P3, P4 > {
    R apply( P1 param1, P2 param2, P3 param3, P4 param4 );
    
    default Function3< R, P2, P3, P4 > reduce1( P1 param1 ) {
        return ( param2, param3, param4 ) -> apply( param1, param2, param3, param4 );
    }
    default Function3< R, P1, P3, P4 > reduce2( P2 param2 ) {
        return ( param1, param3, param4 ) -> apply( param1, param2, param3, param4 );
    }
    default Function3< R, P1, P2, P4 > reduce3( P3 param3 ) {
        return ( param1, param2, param4 ) -> apply( param1, param2, param3, param4 );
    }
    default Function3< R, P1, P2, P3 > reduce4( P4 param4 ) {
        return ( param1, param2, param3 ) -> apply( param1, param2, param3, param4 );
    }
}
