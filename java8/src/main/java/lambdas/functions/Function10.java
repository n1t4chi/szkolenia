/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function10< R, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10 > {
    R apply(
        P1 param1,
        P2 param2,
        P3 param3,
        P4 param4,
        P5 param5,
        P6 param6,
        P7 param7,
        P8 param8,
        P9 param9,
        P10 param10 );
    
    default Function9< R, P2, P3, P4, P5, P6, P7, P8, P9, P10 > reduce1( P1 param1 ) {
        return ( param2, param3, param4, param5, param6, param7, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P3, P4, P5, P6, P7, P8, P9, P10 > reduce2( P2 param2 ) {
        return ( param1, param3, param4, param5, param6, param7, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P4, P5, P6, P7, P8, P9, P10 > reduce3( P3 param3 ) {
        return ( param1, param2, param4, param5, param6, param7, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P5, P6, P7, P8, P9, P10 > reduce4( P4 param4 ) {
        return ( param1, param2, param3, param5, param6, param7, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P4, P6, P7, P8, P9, P10 > reduce5( P5 param5 ) {
        return ( param1, param2, param3, param4, param6, param7, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P4, P5, P7, P8, P9, P10 > reduce6( P6 param6 ) {
        return ( param1, param2, param3, param4, param5, param7, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P4, P5, P6, P8, P9, P10 > reduce7( P7 param7 ) {
        return ( param1, param2, param3, param4, param5, param6, param8, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P4, P5, P6, P7, P9, P10 > reduce8( P8 param8 ) {
        return ( param1, param2, param3, param4, param5, param6, param7, param9, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P4, P5, P6, P7, P8, P10 > reduce9( P9 param9 ) {
        return ( param1, param2, param3, param4, param5, param6, param7, param8, param10 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
    default Function9< R, P1, P2, P3, P4, P5, P6, P7, P8, P9 > reduce10( P10 param10 ) {
        return ( param1, param2, param3, param4, param5, param6, param7, param8, param9 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 );
    }
}
