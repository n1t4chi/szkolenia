/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function8< R, P1, P2, P3, P4, P5, P6, P7, P8 > {
    R apply( P1 param1, P2 param2, P3 param3, P4 param4, P5 param5, P6 param6, P7 param7, P8 param8 );
    
    default Function7< R, P2, P3, P4, P5, P6, P7, P8 > reduce1( P1 param1 ) {
        return ( param2, param3, param4, param5, param6, param7, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P3, P4, P5, P6, P7, P8 > reduce2( P2 param2 ) {
        return ( param1, param3, param4, param5, param6, param7, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P2, P4, P5, P6, P7, P8 > reduce3( P3 param3 ) {
        return ( param1, param2, param4, param5, param6, param7, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P2, P3, P5, P6, P7, P8 > reduce4( P4 param4 ) {
        return ( param1, param2, param3, param5, param6, param7, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P2, P3, P4, P6, P7, P8 > reduce5( P5 param5 ) {
        return ( param1, param2, param3, param4, param6, param7, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P2, P3, P4, P5, P7, P8 > reduce6( P6 param6 ) {
        return ( param1, param2, param3, param4, param5, param7, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P2, P3, P4, P5, P6, P8 > reduce7( P7 param7 ) {
        return ( param1, param2, param3, param4, param5, param6, param8 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
    default Function7< R, P1, P2, P3, P4, P5, P6, P7 > reduce8( P8 param8 ) {
        return ( param1, param2, param3, param4, param5, param6, param7 ) -> apply( param1, param2, param3, param4, param5, param6, param7, param8 );
    }
}
