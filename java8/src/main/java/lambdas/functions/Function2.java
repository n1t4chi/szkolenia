/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function2< R, P1, P2 > {
    R apply( P1 param1, P2 param2 );
    
    default Function1< R, P2 > reduce1( P1 param1 ) {
        return param2 -> apply( param1, param2 );
    }
    
    default Function1< R, P1 > reduce2( P2 param2 ) {
        return param1 -> apply( param1, param2 );
    }
}
