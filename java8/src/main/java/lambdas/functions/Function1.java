/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function1< R, P > {
    R apply( P param );
    
    default Function0< R > reduce( P param ) {
        return () -> apply( param );
    }
    
    default <P1, P2> Function2< R, P1, P2 > unfold( Function2<P, P1, P2> unfold ) {
        return (p1, p2) -> apply( unfold.apply( p1, p2 ) );
    }
}
