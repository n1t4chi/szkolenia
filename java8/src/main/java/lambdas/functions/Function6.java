/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function6< R, P1, P2, P3, P4, P5, P6 > {
    R apply( P1 param1, P2 param2, P3 param3, P4 param4, P5 param5, P6 param6 );
    
    default Function5< R, P2, P3, P4, P5, P6 > reduce1( P1 param1 ) {
        return ( param2, param3, param4, param5, param6 ) -> apply( param1, param2, param3, param4, param5, param6 );
    }
    default Function5< R, P1, P3, P4, P5, P6 > reduce2( P2 param2 ) {
        return ( param1, param3, param4, param5, param6 ) -> apply( param1, param2, param3, param4, param5, param6 );
    }
    default Function5< R, P1, P2, P4, P5, P6 > reduce3( P3 param3 ) {
        return ( param1, param2, param4, param5, param6 ) -> apply( param1, param2, param3, param4, param5, param6 );
    }
    default Function5< R, P1, P2, P3, P5, P6 > reduce4( P4 param4 ) {
        return ( param1, param2, param3, param5, param6 ) -> apply( param1, param2, param3, param4, param5, param6 );
    }
    default Function5< R, P1, P2, P3, P4, P6 > reduce5( P5 param5 ) {
        return ( param1, param2, param3, param4, param6 ) -> apply( param1, param2, param3, param4, param5, param6 );
    }
    default Function5< R, P1, P2, P3, P4, P5 > reduce6( P6 param6 ) {
        return ( param1, param2, param3, param4, param5 ) -> apply( param1, param2, param3, param4, param5, param6 );
    }
}
