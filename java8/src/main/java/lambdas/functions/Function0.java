/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas.functions;

public interface Function0< R > {
    R apply();
}
