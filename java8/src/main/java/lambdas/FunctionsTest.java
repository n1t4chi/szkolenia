/*
 * Copyright (c) 2018 Nokia. All rights reserved.
 */

package lambdas;

import lambdas.functions.Function1;
import lambdas.functions.Function2;
import lambdas.functions.Function6;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class FunctionsTest {
    
    
    
    @Test
    void ReduceTest() {
        Function2<Integer, Integer, Integer> adder = (i,j) -> i+j;
        
        assertValue( 25, adder.apply( 10, 15 ) );
        assertValue( 17, adder.apply( 7, 10 ) );
        
        Function1<Integer,Integer> incrementer = adder.reduce2( 1 );
        
        assertValue( 5, incrementer.apply( 4 ) );
        assertValue( 6, incrementer.apply( 5 ) );
        
        Function2<Integer, Integer, Integer> binaryAdderPlusOne = incrementer.unfold( ( i, j ) -> i+j );
        assertValue( 6, binaryAdderPlusOne.apply( 2,3 ) );
        assertValue( 11, binaryAdderPlusOne.apply( 6,4 ) );
    
        Function6< Vec3, Vec3, Vec3, Vec3, Float, Float, Float> calcPos = this::calculatePosition;
        Function2< Vec3, Vec3, Vec3 > transform =
            calcPos.reduce4( 0f ).reduce4( 0f ).reduce4( 0f ).reduce3( new Vec3( 10, 10, 10 ) );
        
        Assertions.assertEquals( new Vec3( 10,10,10 ), transform.apply( new Vec3( 0,0,0 ), new Vec3( 10,10,10 ) ) );
        Assertions.assertEquals( new Vec3( 21,21,21 ), transform.apply( new Vec3( 2,2,2 ), new Vec3( 1,1,1 ) ) );
    
    }
    
    private Vec3 calculatePosition( Vec3 vertex, Vec3 translation, Vec3 scale, float Rotate0X, float Rotate0Y, float Rotate0Z ) {
        return translate( scale( rotate0X( rotate0Y( rotate0Z( vertex, Rotate0Z ), Rotate0Y), Rotate0X ) , scale ) , translation);
    }
    
    private Vec3 translate( Vec3 vertex, Vec3 translation ) {
        return new Vec3( vertex.x + translation.x, vertex.y + translation.y, vertex.z + translation.z );
    }
    private Vec3 scale( Vec3 vertex, Vec3 scale ) {
        return new Vec3( vertex.x * scale.x, vertex.y * scale.y, vertex.z * scale.z );
    }
    private Vec3 rotate0X( Vec3 vertex, float radians ) {
        Vec2 v = rotate( vertex.y, vertex.z, radians );
        
        return new Vec3( vertex.x, v.x, v.y );
    }
    private Vec3 rotate0Y( Vec3 vertex, float radians ) {
        Vec2 v = rotate( vertex.x, vertex.z, radians );
        return new Vec3( v.x, vertex.y, v.y );
    }
    private Vec3 rotate0Z( Vec3 vertex, float radians ) {
        Vec2 v = rotate( vertex.x, vertex.y, radians );
        return new Vec3( v.x, v.y, vertex.z );
    }
    
    private Vec2 rotate( float x, float y, float radians ) {
        float cos = (float)Math.cos( radians );
        float sin = (float)Math.sin( radians );
        return new Vec2( x * cos - y * sin , x * sin + y * cos );
    }
    
    
    
    class Vec2 {
        final float x;
        final float y;
    
        Vec2( float x, float y ) {
            this.x = x;
            this.y = y;
        }
    }
    class Vec3 {
        final float x;
        final float y;
        final float z;
    
    
        Vec3( float x, float y, float z ) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    
    
        @Override
        public boolean equals( Object o ) {
            if( this == o )
                return true;
            if( o == null || getClass() != o.getClass() )
                return false;
        
            Vec3 vec3 = (Vec3)o;
        
            if( Float.compare( vec3.x, x ) != 0 )
                return false;
            if( Float.compare( vec3.y, y ) != 0 )
                return false;
            return Float.compare( vec3.z, z ) == 0;
        }
    
    
        @Override
        public int hashCode() {
            int result = (x != +0.0f ? Float.floatToIntBits( x ) : 0);
            result = 31 * result + (y != +0.0f ? Float.floatToIntBits( y ) : 0);
            result = 31 * result + (z != +0.0f ? Float.floatToIntBits( z ) : 0);
            return result;
        }
    }
    
    @Test
    void BinaryToUnary() {
        Function2<Integer, Integer, Integer> adder = (i,j) -> i+j;
    
        assertValue( 25, adder.apply( 10, 15 ) );
        assertValue( 17, adder.apply( 7, 10 ) );
    
        Function1<Integer,Integer> incrementer = adder.reduce2( 1 );
        
        assertValue( 5, incrementer.apply( 4 ) );
        assertValue( 6, incrementer.apply( 5 ) );
    }
    
    
    private void assertValue( Integer expected, Integer actual ) {
        Assertions.assertEquals( expected, actual );
    }
}
